package ee.valiit.Kundalini.Yoga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KundaliniYogaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KundaliniYogaApplication.class, args);
	}

}
