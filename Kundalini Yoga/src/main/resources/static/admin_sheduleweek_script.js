console.log ("sheduleweek is working");

// 1. Ül: Alla laadida API'st tekst.

var time_to_string = function (a) {
var time  = new Date(a);
var timeNow = time.getDate() + "." + (time.getMonth()+1) + "."+ time.getFullYear() + "a. ";
var baseUrl = location.origin;
return timeNow
}

var myRefresh = function () {
refreshMessages();
}


var time_to_string_tm = function (a) {
var time  = new Date(a);
var timeNow = time.getDate() + "." + (time.getMonth()+1) + "."+ time.getFullYear() + "a. " +" kell: "+time.getHours()+":"+time.getMinutes();
return timeNow
}


var timeT_to_string = function (a) {
var time  = new Date(a);
var timeNow = time.getHours() + ":" + (time.getMinutes());
return timeNow
}


var refreshMessages = async function () {
    baseUrl = location.origin;

    var timeNow = time_to_string_tm(new Date());

    var weekNr = document.querySelector('#weekSelect').value;
    console.log (weekNr);

    var APIurl = location.origin + "/week/current"

    // fetsh - teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl);
	console.log (request);

	// json () - käsk vormistab meile data mudavaks Jsoniks
    var data = await request.json ();
    console.log (data);
    console.log (data.weekCurrent);
    console.log (data.weekNext);

    var weekCurrent = data.weekCurrent;
    var weekNext = data.weekNext
    var docHtml ="";


    document.querySelector('#test').innerHTML = "Info: Oodake laen andmed ("+ (data.weekCurrent.length)+")";
    document.querySelector('#weektable').innerHTML ="";

if (weekNr==0){

        document.querySelector('#test').innerHTML = "Jooksev nädal: <B>"+ time_to_string(data.weekCurrentstartPeriod) + " - " + time_to_string(data.weekCurrentendPeriod)+"</B>  Täna: "+timeNow;
        docHtml = "<br><br>";
        docHtml +="<table border='1' cellspacing='0' cellpadding='10'>";
        docHtml += "<tr><th width='5%'>ID</th><th width='35%'>Treeningu nimi</th><th width='20%'>Treenerid</th> <th width='35%'>Broneeringu info</th></tr>"

        while (weekCurrent.length > 0) { // kuniks sõnumeid on - VÕIB KA - json.messages.length
        docHtml += " ";

		    var data_row = weekCurrent.shift (); // .pop - last, .shift - esimene
		    console.log (data_row.isToDay);

            if (data_row.lessons_of_day.length > 0) { // IF Tunnid ON!
                docHtml +="<tr><th colspan='4' align='left'>Päev: <B>("+ data_row.dayOfWeek+")</B> " + time_to_string(data_row.dateOfDay)+"";
                	   if(data_row.isToDay){
                	            docHtml +="+";
                		            }
                	docHtml +="</th></tr>";

            }

            while (data_row.lessons_of_day.length > 0) { // Tunnid

             var lesson = data_row.lessons_of_day.shift ();
             var free_palaces = lesson.max_booking - lesson.booking_total;
             docHtml +="<tr><td>"+lesson.id_lesson+"</td><td>"+lesson.name+ " algab: "+ timeT_to_string(lesson.start_time) + " (kestab: "+lesson.duration + " min)</td><td>"+ lesson.teachers+"</td><td>";
             docHtml +=" (veel on "+free_palaces+" vabu kohti)";
             if (free_palaces>0 && !data_row.pastTime ){
             docHtml +="<a href='"+baseUrl+"/booking_form/"+lesson.id_lesson+"'>"+" Lisa "+"</a>"
             } else if (free_palaces==0 && !data_row.pastTime ) {docHtml +="<span style='color: #ff0000;'>&ensp;(grupp on täis!)</span> "}
             if (lesson.booking_total>0){docHtml +=" - broneeritud: <B>"+"<a href='"+baseUrl+"/booking_list/"+lesson.id_lesson+"'>"+lesson.booking_total+"</a>"+"</B>";}
             docHtml +="</td>"
            }
            docHtml +="</tr>"
	}
	docHtml +="</table><br><br>";
document.querySelector('#weektable').innerHTML = docHtml;
}
if (weekNr==1){

     document.querySelector('#test').innerHTML = "Järgmine nädal: <B>"+ time_to_string(data.weekNextstartPeriod) + " - " + time_to_string(data.weekNextendPeriod)+"</B>";
     docHtml = "<br><br>";

      docHtml +="<table border='1' cellspacing='0' cellpadding='10'>";
      docHtml += "<tr><th width='5%'>ID</th><th width='35%'>Treeningu nimi</th><th width='20%'>Treenerid</th> <th width='35%'>Broneeringu info</th></tr>"

        while (weekNext.length > 0) { // kuniks sõnumeid on - VÕIB KA - json.messages.length
         docHtml += "";

		    var data_row = weekNext.shift (); // .pop - last, .shift - esimene
		    console.log (data_row);

    		  if (data_row.lessons_of_day.length > 0) { // IF Tunnid ON!
                            docHtml +="<tr><th colspan='4' align='left'>Päev: <B>("+ data_row.dayOfWeek+")</B> " + time_to_string(data_row.dateOfDay)+"</th></tr>";

                        }

		                while (data_row.lessons_of_day.length > 0) { //
                                 var lesson = data_row.lessons_of_day.shift ();
                                 var free_palaces = lesson.max_booking - lesson.booking_total;
                                 docHtml +="<tr><td>"+lesson.id_lesson+"</td><td>"+lesson.name+ " algab: "+ timeT_to_string(lesson.start_time) + " (kestab: "+lesson.duration + " min)</td><td>"+ lesson.teachers+"</td><td>";
                                 if (free_palaces>0){
                                 docHtml +="<a href='"+ baseUrl+"/booking_form/"+lesson.id_lesson+"'>"+" Lisa "+"</a> (veel on "+free_palaces+" vabad kohad)"
                                 } else {docHtml +="<span style='color: #ff0000;'>&ensp;(grupp on täis!)</span> "}
                                 if (lesson.booking_total>0){docHtml +=" - broneeritud: <B>"+"<a href='"+baseUrl+"/booking_list/"+lesson.id_lesson+"'>"+lesson.booking_total+"</a>"+"</B>";}
                                 docHtml +="</td>"
                                }
                                docHtml +="</tr>"
	}
	docHtml +="</table><br><br>"
document.querySelector('#weektable').innerHTML =docHtml;
}
}

refreshMessages();


setInterval(refreshMessages,10000); // 1000 on üks sekund
//refreshMessages(weekSelect);