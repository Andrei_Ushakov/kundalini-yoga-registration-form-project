package ee.valiit.Kundalini.Yoga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;


import java.lang.reflect.Array;
import java.text.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;



@SpringBootApplication
public class KundaliniYogaApplication implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(KundaliniYogaApplication.class, args);
	}

	@Autowired
	JdbcTemplate jdbcTemplate;


	@Override
	public void run(String... args) throws Exception {

		//*********************************************************************
		//************************ Внешие запросы
		//*********************************************************************

		//RestTemplate rt = new RestTemplate();
		//String result11 = rt.getForObject("https://jsonplaceholder.typicode.com/todos/1", String.class);
		//System.out.println(result11);
		//System.out.println("----------------------");

		System.out.println( "Config database tables");
		//jdbcTemplate.execute("DROP TABLE IF EXISTS lessons");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS lessons (ID_lessons SERIAL, name TEXT, start_time timestamp, duration INT, ID_location INT, teachers TEXT, max_booking INT)");

		int result = jdbcTemplate.queryForObject(
				"SELECT COUNT(*) FROM lessons", Integer.class);

		System.out.println("Всего в базе: (lessons) "+result);

		String sqlKask = "";

		/*
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		sqlKask = "INSERT INTO lessons (name,start_time,duration,ID_location,teachers,max_booking) VALUES ('meditation','"+date+"',45,1,'Anastassia',3)";
		System.out.println(sqlKask);
		Locale.setDefault(new Locale("et", "EE"));
		System.out.println("-LOCAL "+Locale.getDefault());
		jdbcTemplate.execute(sqlKask);

		//Calendar c = Calendar.getInstance();
		//c.set(2019, 03, 15);

		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-18 08:15:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO lessons (name,start_time,duration,ID_location,teachers,max_booking) VALUES ('meditation','"+(date)+"',45,1,'Anastassia & Yelena',5)";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);

		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-20 17:30:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO lessons (name,start_time,duration,ID_location,teachers,max_booking) VALUES ('meditation','"+(date)+"',45,1,'Anastassia',3)";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);

		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-26 08:15:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO lessons (name,start_time,duration,ID_location,teachers,max_booking) VALUES ('meditation','"+(date)+"',45,1,'Anastassia & Yelena',10)";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);

		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-26 10:30:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO lessons (name,start_time,duration,ID_location,teachers,max_booking) VALUES ('meditation','"+(date)+"',45,1,'Anastassia & Yelena',5)";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);

		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-30 20:15:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO lessons (name,start_time,duration,ID_location,teachers,max_booking) VALUES ('meditation','"+(date)+"',45,1,'Anastassia & Yelena',6)";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);
		*/

		//jdbcTemplate.execute("DROP TABLE IF EXISTS booking");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS booking (ID_Booking SERIAL, ID_Lessons INT, booking_date timestamp, customer_name TEXT, customer_mail TEXT, customer_telefon TEXT, deleted BOOLEAN, accesCode INT)");

		/*
		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-20 17:30:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO booking (ID_Lessons,booking_date,customer_name,customer_mail,customer_telefon) VALUES (2,'"+(date)+"','Andrei Ushakov','andrei@ushakov.eu','+3725222290')";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);

		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-15 10:00:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO booking (ID_Lessons,booking_date,customer_name,customer_mail,customer_telefon) VALUES (2,'"+(date)+"','Marina','marina@gmail.eu','+372555595')";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);

		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-03-11 18:00:00") ; //= '2019-03-15 08:15:00';
		sqlKask = "INSERT INTO booking (ID_Lessons,booking_date,customer_name,customer_mail,customer_telefon) VALUES (5,'"+(date)+"','Marina','marina@gmail.eu','+372555595')";
		System.out.println(sqlKask);
		jdbcTemplate.execute(sqlKask);
		*/
//	*********************************************
//  **********Шаблон нередьного расписания укоков
//	*********************************************

		//jdbcTemplate.execute("DROP TABLE IF EXISTS templateLessons");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS templateLessons (ID_template SERIAL,day_week INT, name TEXT,hour_start INT,minut_start INT, duration INT, ID_location INT, teachers TEXT, max_booking INT, delay timestamp, stop_date timestamp)");

		result = jdbcTemplate.queryForObject(
				"SELECT COUNT(*) FROM templateLessons", Integer.class);

		if (result == 0) { //Если шаблон расписания пустой, то создадим

//	*********************************************
//  ********** ПОНЕДЕЛЬНИК - 1
//	*********************************************

			sqlKask = "INSERT INTO templateLessons (day_week, name ,hour_start ,minut_start, duration, ID_location, teachers, max_booking)";
			sqlKask += " VALUES (1,'Grupi Jooga tund',18,30,90,1,'Anastassia (Russian)',8)";
			jdbcTemplate.execute(sqlKask);

//	*********************************************
//  ********** ВТОРНИК - 2
//	*********************************************

			sqlKask = "INSERT INTO templateLessons (day_week, name ,hour_start ,minut_start, duration, ID_location, teachers, max_booking)";
			sqlKask += " VALUES (2,'Grupi Jooga tund',9,30,150,1,'Anastassia ning Jelena (Russian, English)',8)";
			jdbcTemplate.execute(sqlKask);

//	*********************************************
//  ********** Среда - 3
//	*********************************************

			sqlKask = "INSERT INTO templateLessons (day_week, name ,hour_start ,minut_start, duration, ID_location, teachers, max_booking)";
			sqlKask += " VALUES (3,'Grupi Jooga tund',18,30,90,1,'Anastassia (Russian)',8)";
			jdbcTemplate.execute(sqlKask);

//	*********************************************
//  ********** ПЯТНИЦА - 5
//	*********************************************

			sqlKask = "INSERT INTO templateLessons (day_week, name ,hour_start ,minut_start, duration, ID_location, teachers, max_booking)";
			sqlKask += " VALUES (5,'Grupi Jooga tund (Ettetellimisel!)',18,30,90,1,'Yelena tel. +372 521 6265, Anastassia tel. +372 516 1341',8)";
			jdbcTemplate.execute(sqlKask);

//	*********************************************
//  ********** СУББОТА - 6
//	*********************************************

			sqlKask = "INSERT INTO templateLessons (day_week, name ,hour_start ,minut_start, duration, ID_location, teachers, max_booking)";
			sqlKask += " VALUES (6,'Grupi Jooga tund (Ettetellimisel!)',11,0,90,1,'Yelena tel. +372 521 6265, Anastassia tel. +372 516 1341',8)";
			jdbcTemplate.execute(sqlKask);

//	*********************************************
//  ********** ВОСКРЕСЕНЬЕ - 7
//	*********************************************

			sqlKask = "INSERT INTO templateLessons (day_week, name ,hour_start ,minut_start, duration, ID_location, teachers, max_booking)";
			sqlKask += " VALUES (7,'Zen Meditation (Ettetellimisel!)',9,0,90,1,'Yelena tel. +372 521 6265, Anastassia tel. +372 516 1341',8)";
			jdbcTemplate.execute(sqlKask);

			result = jdbcTemplate.queryForObject(
					"SELECT COUNT(*) FROM templateLessons", Integer.class);

			System.out.println("Создан шаблон раписания занатий: (таблици templateLessons) " + result + " шт.");
		}


	}

}
