package ee.valiit.Kundalini.Yoga;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Principal;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure (HttpSecurity http) throws Exception{

        http
                .authorizeRequests()
                .antMatchers("/", "/index", "/css/*","/pic/*","/kundalini_flaier.jpg","/week_shedule.html","/week_shedule.js","/week/current","/do_booking/*","/booking_form/*","pic/logoSmall_Transparent.png").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().permitAll()
                .and()
                .logout().permitAll()
                .and().csrf().disable();//


    }


    @Autowired
    public void configureGlobal (AuthenticationManagerBuilder auth) throws Exception {

        //Prior to Spring Security 5.0 the default PasswordEncoder was NoOpPasswordEncoder which required plain text passwords.
        // In Spring Security 5, the default is DelegatingPasswordEncoder, which required Password Storage Format.
        //Solution 1 – Add password storage format, for plain text, add {noop}
        auth.inMemoryAuthentication().withUser("user").password("{noop}ValiIT2019").roles("USER")
        .and()
        .withUser("admin").password("{noop}ValiIT2019").roles("ADMIN");;

        //Solution 2 – User.withDefaultPasswordEncoder() for UserDetailsService
        //
        //    @Bean
        //    public UserDetailsService userDetailsService() {
        //
        //        User.UserBuilder users = User.withDefaultPasswordEncoder();
        //        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        //        manager.createUser(users.username("user").password("password").roles("USER").build());
        //        manager.createUser(users.username("admin").password("password").roles("USER", "ADMIN").build());
        //        return manager;
        //
        //    }

    }

    public Principal getUserPrincipal() {
        Authentication auth = getAuthentication();

        if ((auth == null) || (auth.getPrincipal() == null)) {
            return null;
        }
        return auth;
    }

    //And the getAuthentication
    private Authentication getAuthentication() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

         if (auth.isAuthenticated()) {return auth;}
        return null;
    }

    public String getCurrentUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    public String getCurrentSessionId(){
        Authentication auth = getAuthentication();
       return auth.getDetails().toString();
    }
}
