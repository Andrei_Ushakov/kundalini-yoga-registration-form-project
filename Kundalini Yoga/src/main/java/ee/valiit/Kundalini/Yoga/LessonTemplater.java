package ee.valiit.Kundalini.Yoga;

import java.util.Date;

public class LessonTemplater {
//(ID_template SERIAL,day_week INT, name TEXT,hour_start INT,minut_start INT, duration INT, ID_location INT, teachers TEXT, max_booking INT, delay timestamp, stop_date timestamp)");

    private int ID_template;
    private int day_week;
    private String name;
    private int hour_start;
    private int minut_start;
    private int duration;
    private int ID_location;
    private String teachers;
    private int max_booking;
    Date delay;
    Date stop_date;

    public LessonTemplater(){}


    public LessonTemplater(int ID_template, int day_week, String name, int hour_start, int minut_start, int duration, int ID_location, String teachers, int max_booking, Date delay, Date stop_date) {
        this.ID_template = ID_template;
        this.day_week = day_week;
        this.name = name;
        this.hour_start = hour_start;
        this.minut_start = minut_start;
        this.duration = duration;
        this.ID_location = ID_location;
        this.teachers = teachers;
        this.max_booking = max_booking;
        this.delay = delay;
        this.stop_date = stop_date;
    }




    public int getID_template() {
        return ID_template;
    }

    public int getDay_week() {
        return day_week;
    }

    public String getName() {
        return name;
    }

    public int getHour_start() {
        return hour_start;
    }

    public int getMinut_start() {
        return minut_start;
    }

    public int getDuration() {
        return duration;
    }

    public int getID_location() {
        return ID_location;
    }

    public String getTeachers() {
        return teachers;
    }

    public int getMax_booking() {
        return max_booking;
    }

    public Date getDelay() {
        return delay;
    }

    public Date getStop_date() {
        return stop_date;
    }


    @Override
    public String toString() {
        return "LessonTemplater{" +
                "ID_template=" + ID_template +
                ", day_week=" + day_week +
                ", name='" + name + '\'' +
                ", hour_start=" + hour_start +
                ", minut_start=" + minut_start +
                ", duration=" + duration +
                ", ID_location=" + ID_location +
                ", teachers='" + teachers + '\'' +
                ", max_booking=" + max_booking +
                ", delay=" + delay +
                ", stop_date=" + stop_date +
                '}';
    }
}
