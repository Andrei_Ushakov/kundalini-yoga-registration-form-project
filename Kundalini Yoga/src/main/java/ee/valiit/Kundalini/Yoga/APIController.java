package ee.valiit.Kundalini.Yoga;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import java.util.*;


//****************************************************************************
//@RestController - Thymeleaf с RestController не работает
// его можно заменить просто @Controller и где надо добавить ещё @ResponseBody
//***************************************************************************
@Controller
@CrossOrigin
public class APIController {



    /// ***********TH****************
    @Value("${welcome.message}")
    private String message;

     /// ***********TH - END****************




   @Autowired                       //Käsk: ühendab ära jdbc klassi - ( ühenda Jdcb dependeci ühendus andmebaasiga)
   JdbcTemplate jdbcTemplate;       //Esimene klass, teine muutuja!!

    @RequestMapping("/booking_form_OLD/{ID_less}")
    @ResponseBody String index(@PathVariable String ID_less) {




        String html_result ="  <head><meta charset='utf-8'><title>Reegistreerimis vorm tunnile nr: "+ID_less+
                "</title><link href=\"../css/style.css\" rel=\"stylesheet\"/>";
        // <link href="../css/bootstrap.min.css" rel="stylesheet"/></head>
        html_result +=" <body><h3>Reegistreerimis vorm tunnile nr: "+ID_less+"</h3>";
        html_result +="<form role='form' method='post' body='JSON.srtingify' action='/do_booking/"+ID_less+"'>";
        html_result +="<input type='text' class='form-control' name='customer_name' placeholder='Teie nimi' required maxlength='50'>";
        html_result +="<input type='text' class='form-control' name='customer_mail' placeholder='e-mail' required maxlength='50'>";
        html_result +="<input type='text' class='form-control' name='customer_telefon' placeholder='Telefon' value='+372' required maxlength='50'>";
        html_result +="<input type='submit' placeholder='Broneeri'></form>";
        html_result +="</body>";

        return html_result;
    }


    //*********************************************************************************************************
    //*******************************BOOKING FORM
    //*********************************************************************************************************

   @RequestMapping("/booking_form/{ID_less}")
   String index(Model model, @PathVariable String ID_less) {

       String sqlKask = "SELECT *, (SELECT count(*) from booking where lessons.ID_Lessons=booking.ID_Lessons) as booking_total from lessons where ID_Lessons="+Integer.parseInt(ID_less);

       ArrayList<Lesson> lessons = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
           String name = (resultSet.getString("name"));
           int id_lessons = (resultSet.getInt("id_lessons"));
           Date start_time = (resultSet.getTimestamp("start_time"));
           int duration = (resultSet.getInt("duration"));
           int id_location = (resultSet.getInt("ID_location"));
           String teachers = (resultSet.getString("teachers"));
           int booking_total = (resultSet.getInt("booking_total"));
           int max_booking = (resultSet.getInt("max_booking"));

           return new Lesson (name,id_lessons,start_time,duration,id_location,teachers,booking_total,max_booking);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
       });

       String lessonName = lessons.get(0).getName();
       Date lessonStart_time = lessons.get(0).getStart_time();
       String lessonTeachers = lessons.get(0).getTeachers();
       int lessonBooking_total = lessons.get(0).getBooking_total();
       int maxBooking = lessons.get(0).getMax_booking();

       String httpPath ="/do_booking/" + ID_less; // нажатие на SUBMIT подтверждение формы бпонирования

       model.addAttribute("httpPath", httpPath);
       model.addAttribute("lessonName", lessonName);
       model.addAttribute("lessonStart_time", lessonStart_time);


        return "booking_form";
    }

    //*********************************************************************************************************
    //*******************************ADMIN - BOOKING LIST
    //*********************************************************************************************************

    @GetMapping("/booking_list/{ID_less}")   // ПРОВЕРКА
    public String requestBookingSql (Model model, @PathVariable String ID_less){

        SecurityConfig user = new SecurityConfig();

        ArrayList<Booking> bookings = requestBooking(ID_less);
        ArrayList<Lesson> lessons = requestLesson(ID_less);

        String userName = new String();
        String task = new String();
        String lingBack = new String();
        String subject = new String();
        Date toDay = new Date();

        userName = user.getCurrentUsername();
        task = "Treeningu andmete päring: ";
        lingBack ="/admin_sheduleweek.html";
        subject = message;

        model.addAttribute("task", task);
        model.addAttribute("bookings", bookings);
            model.addAttribute("lessons", lessons);
        model.addAttribute("lingBack", lingBack);
        model.addAttribute("today", toDay);
        model.addAttribute("userName", userName);
        model.addAttribute("subject", subject); // for mail



           return "admin_list_of_biiking";
        }

    //*********************************************************************************************************
    //*******************************
    //*********************************************************************************************************
    @PostMapping("/do_bookingOLD/{ID_less}")
    @ResponseBody public String index(@ModelAttribute ("customer_name") String customer_name, @ModelAttribute("customer_mail") String customer_mail,
                        @ModelAttribute ("customer_telefon") String customer_telefon, @PathVariable String ID_less) {
    //public RedirectView index(@ModelAttribute Booking newOrdrer, @PathVariable String ID_less) {
        Booking newOrdrer = new Booking(customer_name,customer_mail,customer_telefon);

        Date date = new Date();
        int accesCode =  Integer.parseInt(ID_less) * 1000+((int)(Math.random()*1000));

       String sqlKask = "INSERT INTO booking (customer_name, customer_mail, customer_telefon, booking_date, ID_Lessons, accesCode) VALUES ('" +
                newOrdrer.getCustomer_name() +  "', '" +
                newOrdrer.getCustomer_mail() +  "', '"+
                newOrdrer.getCustomer_telefon() +  "', '"+
                date +  "', '"+
                ID_less +  "', '"+
                accesCode +"')";

        String htmlResult = ("<p style=\"text-align: center;\"> Dear "+newOrdrer.getCustomer_name()+
                " mail:"+newOrdrer.getCustomer_mail()+" phone:"+newOrdrer.getCustomer_telefon() +
                "<br>Your registration is confirmed to lesson ID: "+ID_less+" <br> Your acces code is: "+accesCode+"<br>----------------------------"+
                "<br><a href=\"/\">go to homepage</a><br><div style=\"text-align: center;\"><form action=\"/\"><button type=\"button\">Send Me by mail!</button></form></div></p>");
        jdbcTemplate.execute(sqlKask);
        //****

        //****
        return htmlResult ;
       // return new RedirectView("/success");
    }
    //*********************************************************************************************************
    //*******************************DATA for JAVA Script
    //*********************************************************************************************************

    //@RestController - Thymeleaf с RestController не работает
     // его можно заменить просто @Controller и где надо добавить ещё @ResponseBody
    @GetMapping("/week/current") //SELECT * FROM messages on käsk mis tagastab meile ridu andmebaasist   //Front end teeb päringu sellel aadressilm ning sellel adrel tagastatakse ChatRoom jutuTuba
    @ResponseBody Weeks_shedule result(){

        //********************************************************
        //****************Создаём таблицу дней    ****************
        //********************************************************

        Weeks_shedule a = new Weeks_shedule();

        //********************************************************
        //****************Подключаем данные из базы***************
        //********************************************************

        try {
            String sqlKask = "SELECT *, (SELECT count(*) from booking where lessons.ID_Lessons=booking.ID_Lessons) as booking_total  FROM lessons \n" +
                    "ORDER BY start_time";

            ArrayList<Lesson> lessons = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
                String name = (resultSet.getString("name"));
                int id_lessons = (resultSet.getInt("id_lessons"));
                Date start_time = (resultSet.getTimestamp("start_time"));
                int duration = (resultSet.getInt("duration"));
                int id_location = (resultSet.getInt("ID_location"));
                String teachers = (resultSet.getString("teachers"));
                int booking_total = (resultSet.getInt("booking_total"));
                int max_booking = (resultSet.getInt("max_booking"));

                return new Lesson (name,id_lessons,start_time,duration,id_location,teachers,booking_total,max_booking);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
            });

            // *******************************************************
            // ********Здесь будем отправлять данные в объект Day ****
            // *******************************************************
            int [] haveLessons;
            haveLessons = a.setDateForImport(lessons);

            if (haveLessons [0]==0){// Если не обноружено уроков в текущеё неделе то копируем их из шаблона

                lessobBuilder(0,templateLoader());
            }

            if (haveLessons [1]==0){// Если не обноружено уроков на следующей неделе то копируем их из шаблона

                   lessobBuilder(1,templateLoader());

            }

        } catch (DataAccessException err) { // err - это произвольная переменная (можно назвать как угодно
            System.out.println(" ERROR # "+err);

           // return new ArrayList<>(); // ************* IF ERR!
        }
        return  a;
    }


    //*********************************************************************************************************
    //*******************************INDEX - START
    //*********************************************************************************************************

    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    String index(Model model) {

        SecurityConfig user = new SecurityConfig();

        model.addAttribute("message", message);

        return "index";
    }


    //*************************************************************************************************
    //******************** ADMIN
    //*************************************************************************************************
    @RequestMapping(value = {"/admin" }, method = RequestMethod.GET)
    String adminPage (Model model) {

        SecurityConfig user = new SecurityConfig();

        System.out.println("SEC getCurrentUsername: "+user.getCurrentUsername());
        System.out.println("SEC getUserPrincipal: "+user.getUserPrincipal());
        System.out.println("SEC getUserPrincipal: "+user.getCurrentSessionId());


        model.addAttribute("message", message);
        model.addAttribute("user", user.getCurrentUsername());

        return "admin";
    }


    @PostMapping("/do_booking/{ID_less}")
    public String bookingDone(Model model,@ModelAttribute ("customer_name") String customer_name, @ModelAttribute("customer_mail") String customer_mail,
                                      @ModelAttribute ("customer_telefon") String customer_telefon, @PathVariable String ID_less) throws IOException, MessagingException {
        //public RedirectView index(@ModelAttribute Booking newOrdrer, @PathVariable String ID_less) {
        Booking newOrdrer = new Booking(customer_name,customer_mail,customer_telefon);

        String sqlKask = "SELECT *, (SELECT count(*) from booking where lessons.ID_Lessons=booking.ID_Lessons) as booking_total from lessons where ID_Lessons="+Integer.parseInt(ID_less);

        ArrayList<Lesson> lessons = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
            String name = (resultSet.getString("name"));
            int id_lessons = (resultSet.getInt("id_lessons"));
            Date start_time = (resultSet.getTimestamp("start_time"));
            int duration = (resultSet.getInt("duration"));
            int id_location = (resultSet.getInt("ID_location"));
            String teachers = (resultSet.getString("teachers"));
            int booking_total = (resultSet.getInt("booking_total"));
            int max_booking = (resultSet.getInt("max_booking"));

            return new Lesson (name,id_lessons,start_time,duration,id_location,teachers,booking_total,max_booking);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
        });



        Date date = new Date();
        int accesCode =  Integer.parseInt(ID_less)*10000 + (lessons.get(0).getBooking_total()* 1000)+((int)(Math.random()*1000));


        String sqlKaskBooking = "INSERT INTO booking (customer_name, customer_mail, customer_telefon, booking_date, ID_Lessons, accesCode) VALUES ('" +
                newOrdrer.getCustomer_name() +  "', '" +
                newOrdrer.getCustomer_mail() +  "', '"+
                newOrdrer.getCustomer_telefon() +  "', '"+
                date +  "', '"+
                ID_less +  "', '"+
                accesCode +"')";


        model.addAttribute("customer_name", newOrdrer.getCustomer_name());
        model.addAttribute("customer_mail", newOrdrer.getCustomer_mail());
        model.addAttribute("customer_telefon", newOrdrer.getCustomer_telefon());

        String lessonName = lessons.get(0).getName();
        Date lessonStart_time = lessons.get(0).getStart_time();
        String lessonTeachers = lessons.get(0).getTeachers();
        int lessonBooking_total = lessons.get(0).getBooking_total();
        int maxBooking = lessons.get(0).getMax_booking();

        model.addAttribute("lessonName", lessonName);
        model.addAttribute("lessonStart_time", lessonStart_time);
        model.addAttribute("lessonTeachers", lessonTeachers);
        model.addAttribute("lessonBooking_total", lessonBooking_total);
        model.addAttribute("accesCode", accesCode);

        if ((lessonBooking_total+1)>maxBooking) {
            return "lesson_full" ;
        }

        jdbcTemplate.execute(sqlKaskBooking);

        model.addAttribute("lessonBooking_total", lessonBooking_total+1);

        String emailHTML = "<p>Tere <b>"+customer_name+", </b></p><p>Sa oled registreeritud <b>"+lessonName+"</b> tunnile,  treeneri nimi on "+lessonTeachers+"</p>";
        emailHTML += "<p> Tund algab: <b>"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lessonStart_time) +"</b> ja kestab: <b>"+lessons.get(0).getDuration()+"</b> minuti</p>";
        emailHTML += "Teie acces code: <b>"+accesCode+"</b>";

        Email.send(newOrdrer.getCustomer_mail(), "Registreerimise kinnitus!", emailHTML);
        return "booking_done" ;

    }

    //*************************************************************
    //*************************************************************
    //*************************  постоянные SQL запросы ***********
    //*************************************************************
    //*************************************************************

    //*************************************************************
    // 1. загрузка шаблона расписания
    //*************************************************************
    public  ArrayList<LessonTemplater> templateLoader(){

    String sqlKask = "SELECT * FROM templatelessons";
    ArrayList<LessonTemplater> temlate = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
        int ID_template = (resultSet.getInt("ID_template"));
        int day_week = (resultSet.getInt("day_week"));
        String name = (resultSet.getString("name"));
        int hour_start = (resultSet.getInt("hour_start"));
        int minut_start = (resultSet.getInt("minut_start"));
        int duration = (resultSet.getInt("duration"));
        int id_location = (resultSet.getInt("ID_location"));
        String teachers = (resultSet.getString("teachers"));
        int max_booking = (resultSet.getInt("max_booking"));
        Date delay = (resultSet.getTimestamp("delay"));
        Date stop_date = (resultSet.getTimestamp("stop_date"));

        //System.out.println("************* endid_lessons "+id_lessons + " " + name);

        return new LessonTemplater (ID_template,day_week,name,hour_start,minut_start,duration,id_location,teachers,max_booking,delay,stop_date);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
    });

    return temlate;
    }


    public void lessobBuilder(int weekNr, ArrayList<LessonTemplater> template){

        String sqlKask = new String();

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        int currentWeekNr = calendar.get(Calendar.WEEK_OF_YEAR);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.WEEK_OF_YEAR,currentWeekNr+weekNr);
        System.out.println("DATE before CIKLi - "+calendar.getTime());


        //int calenderOffSet = -(currentDayOfWeek-1);


        for (LessonTemplater eachTemplate: template){


         //   calendar.set(Calendar.HOUR,0);
         //   calendar.set(Calendar.MINUTE,0);

            String name = eachTemplate.getName();
            int duration = eachTemplate.getDuration();
            int id_location = eachTemplate.getID_location();
            String teachers = eachTemplate.getTeachers();
            int booking_total=0;
            int id_lessons=0;
            int max_booking = eachTemplate.getMax_booking();

            System.out.println("DATE1 - "+calendar.getTime()+" get: "+ calendar.get(Calendar.DAY_OF_WEEK));

            int weekDayOffSet = eachTemplate.getDay_week()+1;

            if (weekDayOffSet ==8){
                weekDayOffSet =1;
            }

            int hours=eachTemplate.getHour_start();
            //if (hours >12) { hours+=12; }

            calendar.set(Calendar.DAY_OF_WEEK,weekDayOffSet);
            calendar.set(Calendar.HOUR_OF_DAY,hours);
            calendar.set(Calendar.MINUTE,eachTemplate.getMinut_start());
            calendar.set(Calendar.SECOND,0);
            Date start_time = calendar.getTime();

            sqlKask = "INSERT INTO lessons (name,start_time,duration,ID_location,teachers,max_booking)" +
                    " VALUES ('"+name+"','"+(start_time)+"',"+duration+","+id_location+",'"+teachers+"',"+max_booking+")";
            jdbcTemplate.execute(sqlKask);
        }
    }

    //*************************************************************
    // 2. загрузка броней на урок ID_less усли ID_less=0 то выдаёт все уроки
    //*************************************************************


    public ArrayList<Booking> requestBooking (String ID_less) {
    try {

        String sqlKask = "SELECT * FROM booking JOIN lessons ON lessons.ID_lessons=booking.ID_lessons where booking.ID_lessons ='"+ID_less+"'";  //SQL käsk mis võrdub = käsk! mis küsib andmeid baasist
        if(ID_less.equals("0")) {sqlKask = "SELECT * FROM booking JOIN lessons ON lessons.ID_lessons=booking.ID_lessons";}


        ArrayList<Booking> bookings = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
            int ID_Booking  = (resultSet.getInt("ID_Booking"));
            int ID_Lessons = (resultSet.getInt("ID_Lessons"));
            Date booking_date = (resultSet.getTimestamp("booking_date"));
            String  customer_name = (resultSet.getString("customer_name"));
            String customer_mail = (resultSet.getString("customer_mail"));
            String customer_telefon = (resultSet.getString("customer_telefon"));
            boolean deleted = (resultSet.getBoolean("deleted"));

            return new Booking (ID_Booking,ID_Lessons,booking_date,customer_name,customer_mail,customer_telefon,deleted);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
        });
        return bookings;

    } catch (DataAccessException err) { // err - это произвольная переменная (можно назвать как угодно
        System.out.println(" ERROR # "+err);

        return new ArrayList<>();
    }
    }

    //*************************************************************
    // 3. загрузка урокa ID_less усли ID_less=0 то выдаёт все уроки
    //*************************************************************
    public ArrayList<Lesson> requestLesson (String ID_less) {

    String sqlKask = "SELECT *, (SELECT count(*) from booking where lessons.ID_Lessons=booking.ID_Lessons) as booking_total from lessons where ID_Lessons="+Integer.parseInt(ID_less);

        if(ID_less.equals("0")) {sqlKask = "SELECT *, (SELECT count(*) from booking where lessons.ID_Lessons=booking.ID_Lessons) as booking_total from lessons";}

        ArrayList<Lesson> lessons = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
        String name = (resultSet.getString("name"));
        int id_lessons = (resultSet.getInt("id_lessons"));
        Date start_time = (resultSet.getTimestamp("start_time"));
        int duration = (resultSet.getInt("duration"));
        int id_location = (resultSet.getInt("ID_location"));
        String teachers = (resultSet.getString("teachers"));
        int booking_total = (resultSet.getInt("booking_total"));
        int max_booking = (resultSet.getInt("max_booking"));

        return new Lesson (name,id_lessons,start_time,duration,id_location,teachers,booking_total,max_booking);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
    });
        return lessons;
    }
}
