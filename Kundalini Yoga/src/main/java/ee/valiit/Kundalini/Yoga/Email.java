package ee.valiit.Kundalini.Yoga;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

public class Email {

  //spring.mail.host=smtp.gmail.com
    //spring.mail.port=587
    //spring.mail.username=<login user to smtp server>
    //spring.mail.password=<login password to smtp server>
    //spring.mail.properties.mail.smtp.auth=true
    //spring.mail.properties.mail.smtp.starttls.enable=true




    public static void send(String emailTo, String subject, String msgHTML) throws AddressException, MessagingException, IOException {

      //  System.out.println("send_mail_smtp: "+ send_mail_smtp);
       // System.out.println(" mailport   : "+ mailport   );



        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587"    );

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("kundalinistudio2019@gmail.com", "Katerina!");  // mitte see millega sisse logid
            }
        });


        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("kundalinistudio2019@gmail.com", false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
        msg.setSubject(subject);
        msg.setContent(msgHTML, "text/html");
        msg.setSentDate(new Date());

        Transport.send(msg);
        System.out.println("Email sent");
    }
}