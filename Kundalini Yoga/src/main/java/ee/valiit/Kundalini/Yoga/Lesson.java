package ee.valiit.Kundalini.Yoga;


import java.util.Date;

public class Lesson {
    // Database lessons (name,start_time,duration,ID_location,teachers)";

    private int id_lessons;
    private String name;
    private Date start_time;
    private int duration;
    private int id_location;
    private String teachers;
    private int booking_total;
    private int max_booking;


    public Lesson(){}


    public Lesson(String name, int id_lessons, Date start_time, int duration, int id_location, String teachers, int booking_total,int max_booking) {
        this.name = name;
        this.id_lessons = id_lessons;
        this.start_time = start_time;
        this.duration = duration;
        this.id_location = id_location;
        this.teachers = teachers;
        this.booking_total = booking_total;
        this.max_booking = max_booking;
    }


    public int getId_lesson() {
        return id_lessons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getId_location() {
        return id_location;
    }

    public void setId_location(int id_location) {
        this.id_location = id_location;
    }

    public String getTeachers() {
        return teachers;
    }

    public void setTeachers(String teachers) {
        this.teachers = teachers;
    }

    public int getBooking_total() {
        return booking_total;
    }

    public int getMax_booking() {
        return max_booking;
    }

    public void setMax_booking(int max_booking) {
        this.max_booking = max_booking;
    }
}
