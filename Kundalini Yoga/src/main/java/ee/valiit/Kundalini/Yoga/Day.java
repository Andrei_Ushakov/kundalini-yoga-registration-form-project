package ee.valiit.Kundalini.Yoga;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;


import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.*;


public class Day {

    private String dayOfWeek;
    private Date dateOfDay;
    private boolean pastTime;
    private boolean isToDay;
    Date toDay = new Date();


    private ArrayList<Lesson> lessons_of_day = new ArrayList<Lesson>();


    public Day (){}

    public Day (int nr, int weekNr){

        // weekNr - 0 current Week
        // weekNr - 1 Next Week
        this.toDay.setHours(0);
        this.toDay.setMinutes(0);
        this.toDay.setSeconds(0);


            GregorianCalendar calendar = new GregorianCalendar();
            //Calendar calendar = Calendar.getInstance(new Locale("en","UK"));
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            //calendar.set(2019,02,10);

            int currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)-1;
            int currentWeekNr = calendar.get(Calendar.WEEK_OF_YEAR);
            calendar.set(Calendar.WEEK_OF_YEAR,currentWeekNr+weekNr);

            if (currentDayOfWeek == 0 ) {currentDayOfWeek=7;}
            int calenderOffSet = -(currentDayOfWeek-1)+nr;

            calendar.add(Calendar.DAY_OF_WEEK,(calenderOffSet));
            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);
            //calendar.set(Calendar.MILLISECOND,0);
            this.dateOfDay = calendar.getTime();
 /*
            System.out.println("Год: " + calendar.get(Calendar.YEAR));
            System.out.println("Месяц: " + calendar.get(Calendar.MONTH));
            System.out.println("Порядковый номер недели: " + currentDayOfWeek);
            System.out.println("Число: " + calendar.get(Calendar.DAY_OF_MONTH));
            System.out.println("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));
            System.out.println("Часы: " + calendar.get(Calendar.HOUR));
            System.out.println("Минуты: " + calendar.get(Calendar.MINUTE));
            System.out.println("Секунды: " + calendar.get(Calendar.SECOND));
            System.out.println("Миллисекунды: " + calendar.get(Calendar.MILLISECOND));
*/

            switch (nr) {

                case 0: this.dayOfWeek = "Esmaspäev"; //"Esmaspäev";
                        break;

                case 1: this.dayOfWeek = "Teisipäev"; //"Teisipäev";
                        break;

                case 2: this.dayOfWeek = "Kolmapäev"; //"Kolmapäev";
                        break;

                case 3: this.dayOfWeek = "Neljapäev"; //"Neljapäev";
                        break;

                case 4: this.dayOfWeek = "Reede"; //"Reede";
                        break;

                case 5: this.dayOfWeek = "Laupäev"; //"Laupäev";
                        break;

                case 6: this.dayOfWeek = "Pühapäev"; //"Pühapäev";
                        break;
            }


            this.pastTime = (this.dateOfDay.before(toDay));
            this.isToDay = (this.dateOfDay.equals(toDay));

        }

        public void infoSend (){
            System.out.println("\n---------------------------");
            System.out.println("See on: "+this.dayOfWeek );
            System.out.println("Kuupäev on: "+this.dateOfDay );
            System.out.println("toDay -" +toDay);
            System.out.println("Is PASTTIME: "+pastTime);
            System.out.println("Is TODAY: "+isToDay);
            System.out.println("---------------------------");

        }

        public String getDayOfWeek() {
        return this.dayOfWeek;
        }

        public Date getDateOfDay() {
        return this.dateOfDay;
        }

        public boolean getpastTime() {
        return this.pastTime;
        }

        public boolean getisToDay() {
        return this.isToDay;
        }

        public ArrayList<Lesson> getLessons_of_day (){
        return lessons_of_day;
        }


    public void setLessons_of_day(Lesson getLesson) {
        this.lessons_of_day.add(getLesson);
    }
}
