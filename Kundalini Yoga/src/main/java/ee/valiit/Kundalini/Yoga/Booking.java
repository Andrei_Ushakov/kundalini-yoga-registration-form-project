package ee.valiit.Kundalini.Yoga;

import java.util.Date;

public class Booking {
    private int ID_Booking;
    private int ID_Lessons;
    private Date booking_date;
    private String customer_name;
    private String customer_mail;
    private String customer_telefon;
    private boolean deleted;

    public Booking(){}

    public Booking(String customer_name, String customer_mail, String customer_telefon ){
        this.customer_name = customer_name;
        this.customer_mail = customer_mail;
        this.customer_telefon = customer_telefon;
    }

    public Booking(int ID_Booking, int ID_Lessons, Date booking_date, String customer_name, String customer_mail, String customer_telefon, boolean deleted){
        this.ID_Booking = ID_Booking;
        this.ID_Lessons = ID_Lessons;
        this.booking_date = booking_date;
        this.customer_name = customer_name;
        this.customer_mail = customer_mail;
        this.customer_telefon = customer_telefon;
    }


    public int getID_Booking() {
        return ID_Booking;
    }

    public int getID_Lessons() {
        return ID_Lessons;
    }

    public Date getBooking_date() {
        return booking_date;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getCustomer_mail() {
        return customer_mail;
    }

    public String getCustomer_telefon() {
        return customer_telefon;
    }

    public boolean isDeleted() {
        return deleted;
    }
}
