package ee.valiit.Kundalini.Yoga;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Weeks_shedule {

    private ArrayList<Day> weekCurrent = new ArrayList<Day>();
    private ArrayList<Day> weekNext = new ArrayList<Day>();

    private Date weekCurrentstartPeriod;
    private Date weekCurrentendPeriod;

    private Date weekNextstartPeriod;
    private Date weekNextendPeriod;

    public Weeks_shedule (){

        for (int i = 0; i <7 ; i++) {
            weekCurrent.add(new Day(i,0));

            switch (i) {
                    case 0:
                    this.weekCurrentstartPeriod = weekCurrent.get(i).getDateOfDay();
                    break;

                case 6:
                    this.weekCurrentendPeriod = weekCurrent.get(i).getDateOfDay();
                    break;
            }
         }

        for (int i = 0; i <7 ; i++) {
            weekNext.add(new Day(i,1));

            switch (i) {
                case 0:
                    this.weekNextstartPeriod = weekNext.get(i).getDateOfDay();
                    break;

                case 6:
                    this.weekNextendPeriod = weekNext.get(i).getDateOfDay();
                    break;
            }
         }

    }
    public String view_day(int a) {
        return weekCurrent.get(a).getDayOfWeek();
    }
    public Object view_all_days() {
        //String result = Arrays.toString(n);
        return this.weekCurrent;
    }

    public ArrayList<Day> getWeekCurrent() {
        return weekCurrent;
    }

    public ArrayList<Day> getWeekNext() {
        return weekNext;
    }


    public Date getWeekCurrentstartPeriod() {
        return weekCurrentstartPeriod;
    }

    public Date getWeekCurrentendPeriod() {
        return weekCurrentendPeriod;
    }

    public Date getWeekNextstartPeriod() {
        return weekNextstartPeriod;
    }

    public Date getWeekNextendPeriod() {
        return weekNextendPeriod;
    }

    public int[]setDateForImport(ArrayList<Lesson> dateForImport) {

        //*******************************
        //****insert Lessons in Calender
        //*******************************
        int[]haveLessons = {0, 0};
        for (int i = 0; i <7 ; i++) {
           SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
           String curWeekKalenderDate = format.format(this.weekCurrent.get(i).getDateOfDay());
            String nextWeekKalenderDate = format.format(this.weekNext.get (i).getDateOfDay());

            for (Lesson eachLesson: dateForImport)
            {
                String lessonDate = format.format(eachLesson.getStart_time());
                  boolean equalDate = lessonDate.equals(curWeekKalenderDate);
                    if (equalDate){
                        this.weekCurrent.get(i).setLessons_of_day(eachLesson);
                        haveLessons[0]++;
                     }

                equalDate = lessonDate.equals(nextWeekKalenderDate);
                if (equalDate){
                    this.weekNext.get(i).setLessons_of_day(eachLesson);
                    haveLessons[1]++;
                }
            }


        }

    return haveLessons;

    }
}

